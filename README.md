# Let's Aggregate Superb Toots

![Last logo: a viking hat](themes/default/img/favicon.png)

**To toot all my favourites toots!**




# Software

This works thanks to [Last](https://framagit.org/luc/last), free-software under the MIT License (see the [LICENSE](LICENSE) file for details). Please, go to <https://luc.frama.io/last/> to see how it looks.

## Author

[Luc Didry](https://fiat-tux.fr).

You can support me on [Tipeee](https://tipeee.com/fiat-tux) ![Tipeee button](themes/default/img/tipeee-tip-btn.png) and [Liberapay](https://liberapay.com/sky). ![Liberapay logo](themes/default/img/liberapay.png)